export const ADD_SHOE = "ADD_SHOE";
export const DELETE_SHOE = "DELETE_SHOE";
export const GET_DETAIL = "GET_DETAIL";
export const CHANGE_QUANTITY = "CHANGE_QUANTITY";
