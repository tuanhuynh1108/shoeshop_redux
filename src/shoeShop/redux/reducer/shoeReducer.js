import { shoeArr } from "../../datashoe/dataShoe";
import {
  ADD_SHOE,
  CHANGE_QUANTITY,
  DELETE_SHOE,
  GET_DETAIL,
} from "../constant/shoeConstant";

let initialState = {
  shoeArr: shoeArr,
  cart: [],
  detailShoe: shoeArr[0],
};
export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_SHOE: {
      let cloneCart = [...state.cart];

      var index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        cloneCart.push({ ...action.payload, soLuong: 1 });
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }
    case GET_DETAIL: {
      return { ...state, detailShoe: action.payload };
    }
    case DELETE_SHOE: {
      let cloneCart = [...state.cart];

      let index = cloneCart.findIndex((shoe) => {
        return shoe.id == action.payload;
      });

      cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    case CHANGE_QUANTITY: {
      console.log(action.payload);

      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      console.log("index: ", index);
      cloneCart[index].soLuong =
        cloneCart[index].soLuong + action.payload.luaChon;
      cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    default: {
      return state;
    }
  }
};
