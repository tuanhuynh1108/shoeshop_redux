import { combineReducers } from "redux";
import { shoeReducer } from "./shoeReducer";

export const rootShoeReducer = combineReducers({ shoeReducer });
