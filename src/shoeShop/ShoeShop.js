import React, { Component } from "react";
import CartShoe from "./CartShoe";
import { shoeArr } from "./datashoe/dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class ShoeShop extends Component {
  // state = {
  //   shoeArr: shoeArr,
  //   cart: [],
  // };
  render() {
    return (
      <div className="row">
        <div className="col-6">
          <CartShoe />
        </div>
        <div className="col-6">
          <ListShoe />
        </div>

        <DetailShoe />
      </div>
    );
  }
}
