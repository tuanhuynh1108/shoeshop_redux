import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.listShoe.map((item, index) => {
      return <ItemShoe dataShoe={item} key={index} />;
    });
  };
  render() {
    return <div className="row ">{this.renderListShoe()}</div>;
  }
}

let mapStatetoProps = (state) => {
  return { listShoe: state.shoeReducer.shoeArr };
};

export default connect(mapStatetoProps)(ListShoe);
