import React, { Component } from "react";
import { connect } from "react-redux";
import { GET_DETAIL } from "./redux/constant/shoeConstant";

class DetailShoe extends Component {
  render() {
    let { name, price, description, image } = this.props.detailShoe;
    return (
      <div>
        {/* <!-- Modal --> */}
        <div
          class="modal fade"
          id="exampleModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  {name}
                </h5>
                <button
                  type="button"
                  class="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body table">
                <thead>
                  <td>{price}</td>
                  <td>{description}</td>
                  <td>
                    <img src={image} style={{ width: 100 }} />
                  </td>
                </thead>
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapStaretoProps = (state) => {
  return { detailShoe: state.shoeReducer.detailShoe };
};
// let mapDispatchToProps = (dispatch) => {
//   handleChangeDetail = (shoe) => {
//     let action = {
//       type: GET_DETAIL,
//       payload: shoe,
//     };
//     dispatch(action);
//   };
// };
export default connect(mapStaretoProps)(DetailShoe);
