import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ADD_SHOE,
  CHANGE_QUANTITY,
  DELETE_SHOE,
} from "./redux/constant/shoeConstant";

class CartShoe extends Component {
  renderCartShoe = () => {
    return this.props.cartShoe.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img src={item.image} style={{ width: 100 }} />
          </td>
          <td>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleChangQuantity(item.id, -1);
              }}
            >
              -
            </button>
            <strong className="mx-2">{item.soLuong}</strong>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleChangQuantity(item.id, 1);
              }}
            >
              +
            </button>
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="table">
        <thead>
          <th>ID</th>
          <th>Name</th>
          <th>Price</th>
          <th>Image</th>
          <th>Quantity</th>
          <th>Action</th>
        </thead>
        <tbody>{this.renderCartShoe()}</tbody>
      </div>
    );
  }
}

let mapStatetoProps = (state) => {
  return {
    cartShoe: state.shoeReducer.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (item) => {
      let action = {
        type: DELETE_SHOE,
        payload: item,
      };
      dispatch(action);
    },
    handleChangQuantity: (id, luaChon) => {
      let action = {
        type: CHANGE_QUANTITY,
        payload: { id, luaChon },
      };
      dispatch(action);
    },
  };
};

export default connect(mapStatetoProps, mapDispatchToProps)(CartShoe);
