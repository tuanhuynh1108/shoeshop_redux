import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_SHOE, GET_DETAIL } from "./redux/constant/shoeConstant";

class ItemShoe extends Component {
  render() {
    return (
      <div className="col-6">
        <div className="card text-left ">
          <img className="card-img-top" src={this.props.dataShoe.image} alt />
          <div className="card-body">
            <h4 className="card-title">{this.props.dataShoe.name}</h4>
            <p className="card-text">{this.props.dataShoe.price}</p>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handlAddProduct(this.props.dataShoe);
              }}
            >
              Add
            </button>

            {/* <!-- Button trigger modal --> */}
            <button
              type="button"
              class="btn btn-warning"
              data-toggle="modal"
              data-target="#exampleModal"
              onClick={() => {
                this.props.handleChangeDetail(this.props.dataShoe);
              }}
            >
              View Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handlAddProduct: (id) => {
      let action = {
        type: ADD_SHOE,
        payload: id,
      };
      dispatch(action);
    },
    handleChangeDetail: (shoe) => {
      let action = {
        type: GET_DETAIL,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
